class Persona:
    def __init__(self, nombre, apellidos, nacimiento, DNI):
        self.nombre = nombre
        self.apellidos = apellidos
        self.nacimiento = nacimiento
        self.DNI = DNI

    def setNombre(self, nombre):
        self.nombre = nombre

    def setApellidos(self, apellidos):
        self.apellidos = apellidos 

    def setNacimiento(self, nacimiento):
        self.nacimiento = nacimiento

    def setDNI(self, DNI):
        self.DNI = DNI

    def getNombre(self):
        return self.nombre

    def getApellidos(self):
        return self.apellidos

    def getNacimiento(self):
        return self.nacimiento

    def getDNI(self):
        return self.DNI

    def __str__(self):
        return f'El paciente/doctor {self.nombre} {self.apellidos}, nacido el {self.nacimiento} y DNI {self.DNI}'
    
class Paciente(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, historial_clinico):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.historial_clinico = historial_clinico

    def VerHistorialClinico(self):
        return self.historial_clinico


   

class Medico(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, especialidad, citas):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.especialidad = especialidad
        self.citas = citas

    def ConsultarAgenda(self):
        return(self.especialidad, self.citas)

if __name__ == '__main__':
    paciente1 = Paciente('Laura', 'Gutierrez Cortés', '3 de enero de 2007', '12345678Q', 'depresión')    
    print(paciente1.VerHistorialClinico())

    doctor1 = Medico('Juan', 'Pérez Cortés', '12 de abril de 1999', '98765432W', 'Oncología', '3/09/2024')
    print(doctor1.ConsultarAgenda())




    
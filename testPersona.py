from persona_clases import Persona, Paciente, Medico
import unittest

class PersonaTestClase(unittest.TestCase):
    def test_VerHistorialClinico(self):
        paciente1 = Paciente('Laura', 'Gutierrez Cortés', '3 de enero de 2007', '12345678Q', 'depresión')
        historial_clinico = paciente1.VerHistorialClinico()
        self.assertEqual (historial_clinico, 'depresión')

    def test_ConsultarCita(self):
        doctor1 = Medico('Juan', 'Pérez Cortés', '12 de abril de 1999', '98765432W', 'Oncología', '3/09/2024')
        citas = doctor1.ConsultarAgenda()
        self.assertEqual (citas, ('Oncología','3/09/2024'))

unittest.main()